#include <iostream>
#include <stack>

using namespace std;

class Stack
{
public:
    void pushStructureInt(int value)
    {
        structureInt.push(value);
    }

    int popStructureInt()
    {
        return structureInt.top();
    }
    
    void deleteTopElement()
    {
        structureInt.pop();
    }
    
    void printTopElement()
    {
        cout << "Top element: " << structureInt.top() << endl;
    }

    void isEmptyStructure()
    {
        if (structureInt.empty())
        {
            cout << "structureInt empty" << endl;
        } else
        {
            cout << "structureInt not empty" << endl;
        }
    }
private:
    stack <int> structureInt;
};


int main(int argc, char* argv[])
{
    Stack* newStructure = new Stack;
    int i = 0;
    
    cout << "Input length structure: " << endl;
    int lengthStructure;
    cin >> lengthStructure;

    newStructure->isEmptyStructure();
    cout << "Input integers. Their number should be equal to length structure: " << endl;
    
    while (i != lengthStructure) {
        int value;
        cin >> value; 
        newStructure->pushStructureInt(value);
        i++;
    }
    
    newStructure->isEmptyStructure();
    newStructure->printTopElement();
    newStructure->deleteTopElement();
    newStructure->printTopElement();

    
    return 0;
}
